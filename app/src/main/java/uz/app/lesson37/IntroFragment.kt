package uz.app.lesson37

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yarolegovich.slidingrootnav.SlidingRootNav
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder
import kotlinx.android.synthetic.main.fragment_intro.view.*
import uz.app.lesson37.databinding.FragmentIntroBinding

@Suppress("UNCHECKED_CAST")
class IntroFragment : Fragment(R.layout.fragment_intro), DrawerAdapter.OnItemSelectedListener {
    private lateinit var screenTitles: Array<String>
    private lateinit var screenDrawables: Array<Drawable?>
    private var slidingRootNav: SlidingRootNav? = null
    private lateinit var binding: FragmentIntroBinding

    companion object {
        const val POS_EXIT =0
        const val POS_MAIN = 1
        const val POS_ACCOUNT = 2
        const val POS_ABOUT = 3
        const val POS_OUR_APPS = 4
        const val POS_SHARE_APPS = 5
        const val POS_LOG_OUT = 7
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        val toolbar = binding.toolbar


        slidingRootNav = SlidingRootNavBuilder(requireActivity())
            .withDragDistance(180)
            .withRootViewScale(0.7f)
            .withRootViewElevation(25)
            .withToolbarMenuToggle(binding.toolbar)
            .withMenuOpened(false)
            .withContentClickableWhenMenuOpened(false)
            .withSavedState(savedInstanceState)
            .withMenuLayout(R.layout.drawer_menu)
            .inject()
        screenDrawables = loadScreenIcons()
        screenTitles = loadScreenTitles()
        val adapter = DrawerAdapter(
            listOf(
                createItemFor(POS_EXIT),
                createItemFor(POS_MAIN),
                createItemFor(POS_ACCOUNT),
                createItemFor(POS_ABOUT),
                createItemFor(POS_OUR_APPS),
                createItemFor(POS_SHARE_APPS),
                createItemFor(POS_LOG_OUT)
            ) as List<DrawerItem<DrawerAdapter.DrawerViewHolder>>
        )
        adapter.setListener(this)
        val rv = requireActivity().findViewById<RecyclerView>(R.id.drawer_list)
        rv.layoutManager = LinearLayoutManager(requireContext())
        rv.adapter = adapter
        adapter.setSelected(0)

    }

    private fun loadScreenTitles(): Array<String> = resources.getStringArray(R.array.screenTitles)

    // array tipdagi drawablelarni olish
    private fun loadScreenIcons(): Array<Drawable?> {
        val typedArray = resources.obtainTypedArray(R.array.screenIcon)
        val icons = arrayOfNulls<Drawable>(typedArray.length())
        for (i in 0 until typedArray.length()) {
            val id = typedArray.getResourceId(i, 0)
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(requireContext(), id)
            }
        }
        typedArray.recycle()
        return icons
    }

    @ColorInt
    private fun color(@ColorRes res:Int):Int{
        return ContextCompat.getColor(requireContext(),res)
    }

    private fun createItemFor(position: Int): DrawerItem<*> {
        return SimpleItem(screenDrawables[position]!!, screenTitles[position])
            .withIconTint(color(R.color.purple_200))
            .withTextTint(color(R.color.black))
            .withSelectIconTint(color(R.color.purple_700))
            .withSelectTextTint(color(R.color.purple_700))


    }

    override fun onItemSelect(position: Int) {
        if (position  == POS_MAIN)
            Toast.makeText(requireContext(), "Asosiy sahifa", Toast.LENGTH_SHORT).show()
        else if (position  == POS_ACCOUNT)
           fragmentManager!!.beginTransaction().replace(binding.fragmentContainer.id,PersonFragment()).commit()
        else if (position  == POS_ABOUT)
            Toast.makeText(requireContext(), "Biz haqimizda", Toast.LENGTH_SHORT).show()
        else if (position == POS_OUR_APPS)
            Toast.makeText(requireContext(), "Biznig dasturlar", Toast.LENGTH_SHORT).show()
        else if (position  == POS_SHARE_APPS)
            Toast.makeText(requireContext(), "Dasturni ulashish", Toast.LENGTH_SHORT).show()
        else if (position  == POS_LOG_OUT-1)
            Toast.makeText(requireContext(), "Dasturdan chiqish", Toast.LENGTH_SHORT).show()

        slidingRootNav!!.closeMenu()
    }
}