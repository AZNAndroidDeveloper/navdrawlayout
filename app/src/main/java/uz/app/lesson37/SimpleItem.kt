package uz.app.lesson37

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SimpleItem(private val icon:Drawable, private val title:String):DrawerItem<SimpleItem.ViewHolder?>() {
    private var selectedItemIconTint = 0
    private var selectedItemTextTint = 0
    private var normalItemIconTint = 0
    private var normalItemTextTint = 0


    override fun createViewHolder(parent: ViewGroup?): ViewHolder? {
        val inflater = LayoutInflater.from(parent?.context)
        val view = inflater.inflate(R.layout.item_options,parent,false)
    return ViewHolder(view)
    }

    override fun bindViewHolder(holder: ViewHolder?) {
holder!!.icon.setImageDrawable(icon)
    holder.title.text = title
        holder.title.setTextColor(if(isChecked) selectedItemTextTint else normalItemTextTint)
        holder.icon.setColorFilter(if (isChecked) selectedItemIconTint else normalItemIconTint)
    }

    class ViewHolder(itemView:View):DrawerAdapter.DrawerViewHolder(itemView){
        val icon:ImageView
        val title:TextView
        init {
            icon =itemView.findViewById(R.id.icon)
            title = itemView.findViewById(R.id.title)
        }

    }

    // ranglarini o'zgartirish
fun withSelectIconTint(selectedItemIconTint:Int):SimpleItem{
this.selectedItemIconTint = selectedItemIconTint
        return this
}

    fun withSelectTextTint(selectedItemTextTint:Int):SimpleItem{
        this.selectedItemTextTint = selectedItemTextTint
        return this
    }

    fun withIconTint(normalIconTint:Int):SimpleItem{
        this.normalItemIconTint = normalIconTint
        return this
    }
    fun withTextTint(normalTextTint:Int):SimpleItem{
        this.normalItemTextTint = normalTextTint
        return this
    }
}