package uz.app.lesson37

import android.view.View
import android.view.ViewGroup

class SpaceItem(val spaceDp:Int):DrawerItem<SpaceItem.SpaceViewHolder>()  {
class SpaceViewHolder(itemView:View):DrawerAdapter.DrawerViewHolder(itemView){}

    override fun createViewHolder(parent: ViewGroup?): SpaceViewHolder {
        val context = parent?.context
        val view = View(context)
        val height = (context?.resources?.displayMetrics!!.density * spaceDp).toInt()
        view.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,height)
        return SpaceViewHolder(view)

    }

    override val isSelectable: Boolean
        get() = false

    override fun bindViewHolder(holder: SpaceViewHolder) {

    }

}