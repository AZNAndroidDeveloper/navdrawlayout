package uz.app.lesson37

import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class DrawerAdapter(protected val items: List<DrawerItem<DrawerAdapter.DrawerViewHolder>>) :
    RecyclerView.Adapter<DrawerAdapter.DrawerViewHolder>() {

    private var viewType: MutableMap<Class<out DrawerItem<*>>, Int?> = HashMap()
    private  var holderFactories: SparseArray<DrawerItem<*>> = SparseArray()
    private var listener: OnItemSelectedListener? = null
init {
    processViewTypes()
}
    private fun processViewTypes() {
        var type = 0
        for (item in items) {
            if (!viewType.containsKey(item.javaClass)) {
                viewType[item.javaClass] = type
                holderFactories.put(type, item)
                type++
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrawerViewHolder {
        val holder = holderFactories[viewType].createViewHolder(parent)
        holder?.drawerAdapter = this
        return holder!!
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: DrawerViewHolder, position: Int) {
        items[position].bindViewHolder(holder)
    }

    abstract class DrawerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        var drawerAdapter: DrawerAdapter? = null
        override fun onClick(v: View?) {
            drawerAdapter!!.setSelected(adapterPosition)
        }
        init {
            itemView.setOnClickListener(this)
        }
    }

    fun setSelected(position: Int) {
        val newCheck = items[position]
        if (!newCheck.isSelectable) {
            return
        }
        for (i in items.indices) {
            val item = items[i]
            if (item.isChecked) {
                item.setItemChecked(false)
                notifyItemChanged(i)
                break
            }
        }
        newCheck.setItemChecked(true)
        notifyItemChanged(position)
        if (listener != null) {
            listener!!.onItemSelect(position)
        }
    }
    fun setListener(listener: OnItemSelectedListener){
        this.listener = listener
    }

    interface OnItemSelectedListener {
        fun onItemSelect(position: Int)
    }
}